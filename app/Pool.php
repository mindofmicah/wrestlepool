<?php

namespace WrestlePool;

use Illuminate\Database\Eloquent\Model;

class Pool extends Model
{
    /**
     * @param Show $show
     * @return Event
     */
    public function addEventForShow(Show $show)
    {
        return $this->events()->create(['show_id' => $show->id]);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

}
