<?php
namespace WrestlePool\Http\Controllers;

class PoolsController extends Controller
{
    public function index()
    {
        $pools = auth()->user()->pools;

        return view('pools.index', compact('pools'));
    }
}