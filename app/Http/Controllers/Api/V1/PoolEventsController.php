<?php

namespace WrestlePool\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use WrestlePool\Event;
use WrestlePool\Http\Controllers\Controller;

class PoolEventsController extends Controller
{
    public function index($pool_id, $event_id)
    {
        $event = Event::find($event_id);

        return response()->json([
            'event' => $event->toState(),
        ]);
    }
}
