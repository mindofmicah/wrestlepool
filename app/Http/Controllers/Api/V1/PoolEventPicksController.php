<?php

namespace WrestlePool\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use WrestlePool\Event;
use WrestlePool\Http\Controllers\Controller;
use WrestlePool\Pool;
use WrestlePool\Show;

class PoolEventPicksController extends Controller
{
    public function store($pool_id, $event_id)
    {
        /** @var Event $event */
        $event = Event::find($event_id);
        $pick = $event->addPickForUser(request('user_id'), request('pick_id'));
        return response()->json([
            'id'         => $pick->id,
            'name'       => $pick->selection->pickable->name,
            'confidence' => $pick->confidence,
        ], 201);
    }

    public function increaseConfidence($pick_id)
    {
        $pick = \WrestlePool\Pick::find($pick_id);
        $pick->increaseConfidence();

        return response()->json([
            'id'=>$pick->id,
            'name'=>$pick->selection->pickable->name,
            'confidence'=>$pick->confidence
        ]);
        dd($pick);
    }
}
