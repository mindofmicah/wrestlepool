<?php

namespace WrestlePool\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $pools = auth()->user()->pools;
        return view('dashboard.index', compact('pools'));
    }
}
