<?php

namespace WrestlePool;

use Illuminate\Database\Eloquent\Model;

class AvailablePick extends Model
{
    protected $guarded = [];

    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    public function pickable()
    {
        return $this->morphTo();
    }
}
