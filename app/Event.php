<?php

namespace WrestlePool;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];

    /**
     * @param $user_id
     * @param $pick_id
     * @return Pick
     */
    public function addPickForUser($user_id, $pick_id)
    {
        $match_count = $this->show->matches()->count();
        $picks_made = $this->picks()->where('user_id', $user_id)->count();

        return $this->picks()->create([
            'user_id'    => $user_id,
            'pick_id'    => $pick_id,
            'confidence' => $match_count - $picks_made,
        ]);
    }

    public function getNameAttribute()
    {
        return $this->show->name;
    }

    public function picks()
    {
        return $this->hasMany(Pick::class);
    }

    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    public function toState()
    {
        return [
            'name'    => $this->name,
            'matches' => $this->show->matches->map(function (Match $match) {
                return [
                    'available_picks' => [
                        'misc'      => [],
                        'wrestlers' => $match->wrestlers->map(function ($wrestler) {
                            return [
                                'name' => $wrestler->pickable->name,
                                'id'   => $wrestler->id,
                            ];
                        })->toArray(),
                    ],

                ];
            })->toArray(),
        ];
    }
}
