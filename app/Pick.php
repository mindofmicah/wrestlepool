<?php
namespace WrestlePool;

use Illuminate\Database\Eloquent\Model;
use WrestlePool\Exceptions\ConfidenceLowerBoundException;

class Pick extends Model
{
    protected $guarded =[];

    public function increaseConfidence()
    {
        $new_confidence = $this->confidence + 1;

        $match_count = $this->event->show->matches()->count();
    
        if ($match_count < $new_confidence) {
            throw new \WrestlePool\Exceptions\ConfidenceUpperBoundException;
        }

        $this->event->picks()->where('confidence', $new_confidence)->update(['confidence'=>$this->confidence]);

        $this->update([
            'confidence' => $new_confidence
        ]);
    }

    public function decreaseConfidence()
    {
        if ($this->confidence == 1) {
            throw new ConfidenceLowerBoundException;
        }

        $this->event->picks()->where('confidence', $this->confidence - 1)->increment('confidence');

        $this->decrement('confidence');
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function selection()
    {
        return $this->belongsTo(AvailablePick::class, 'pick_id');
    }


}
