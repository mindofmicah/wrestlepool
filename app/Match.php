<?php

namespace WrestlePool;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    public function availablePicks()
    {
        return $this->hasMany(AvailablePick::class);
    }

    public function wrestlers()
    {
        return $this->availablePicks()
            ->where('pickable_type', Wrestler::class);
    }
}
