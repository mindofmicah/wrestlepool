<?php

namespace WrestlePool;

use Illuminate\Database\Eloquent\Model;

class Wrestler extends Model
{
    public function availablePicks()
    {
        return $this->morphMany(AvailablePick::class, 'pickable');
    }
}
