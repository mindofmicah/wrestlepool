<?php

namespace WrestlePool;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Show extends Model
{
    protected $fillable = ['name'];

    public function addMatch(Collection $wrestlers)
    {
        $match = $this->matches()->create([]);
        $wrestlers->each(function (Wrestler $wrestler) use ($match) {
            $wrestler->availablePicks()->create([
                'match_id' => $match->id,
            ]);
        });

        return $match;
    }

    public function matches()
    {
        return $this->hasMany(Match::class);
    }

}
