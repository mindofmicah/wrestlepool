<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(WrestlePool\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ? : $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(WrestlePool\Show::class, function (Faker\Generator $faker) {
    return [

    ];
});

$factory->define(WrestlePool\Wrestler::class, function (Faker\Generator $faker) {
    return [
        'name'=>'Fake Wrestler',
    ];
});

$factory->define(WrestlePool\Pool::class, function (Faker\Generator $faker) {
    return [
        'name'=> 'Pool Name'
    ];
});

$factory->define(\WrestlePool\Show::class, function (Faker\Generator $faker) {
    return [
        'name'=> 'Fake Show Name'
    ];
});

$factory->define(\WrestlePool\Event::class, function (Faker\Generator $faker) {
    return [
        'show_id' => function () {
            return factory(WrestlePool\Show::class)->create()->id;
        },
        'pool_id' => function () {
            return factory(WrestlePool\Pool::class)->create()->id;
        }
    ];
});

$factory->define(\WrestlePool\Match::class, function (Faker\Generator $faker) {
    return [
        'show_id'=> function () {
            return factory(WrestlePool\Show::class)->create()->id;
        }
    ];
});

$factory->define(\WrestlePool\AvailablePick::class, function (Faker\Generator $faker) {
    $pickable = factory(WrestlePool\Wrestler::class)->create();
    return [
        'pickable_type' => get_class($pickable),
        'pickable_id'   => $pickable->id,
        'match_id'      => function () {
            return factory(WrestlePool\Match::class)->create()->id;
        }
    ];
});
$factory->define(\WrestlePool\Pick::class, function (Faker\Generator $faker) {
    return [
        'pick_id'=> function () {
            return factory(WrestlePool\AvailablePick::class)->create()->id;
        },
        'user_id'=> function () {
            return factory(WrestlePool\User::class)->create()->id;
        },
        'event_id'=> function () {
            return factory(WrestlePool\Event::class)->create()->id;
        }
    ];
});
