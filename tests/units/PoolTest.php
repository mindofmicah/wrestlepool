<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use WrestlePool\Pool;
use WrestlePool\Show;

class PoolTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_pool_can_add_events()
    {
        // Arrange
        $pool = factory(Pool::class)->create();
        $show = factory(Show::class)->create();

        // Act
        $this->assertEquals(0, $pool->events()->count());
        $event = $pool->addEventForShow($show);

        // Assert
        $this->assertInstanceOf(\WrestlePool\Event::class, $event);
        $this->assertEquals(1, $pool->events()->count());
    }
}
