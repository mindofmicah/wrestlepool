<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use WrestlePool\User;

class UserTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function a_user_can_join_pools()
    {
        // Arrange
        $user = factory(User::class)->create();
        $this->assertEquals(0, $user->pools()->count());

        // Act
        $user->joinPool(factory(\WrestlePool\Pool::class)->create());

        // Assert
        $this->assertEquals(1, $user->pools()->count());
    }
}
