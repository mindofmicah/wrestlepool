<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PickTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function it_can_increase_the_confidence()
    {
        // Arrange
        $user = factory(WrestlePool\User::class)->create();

        $event = $this->createEventWithMatches(2);
        $matches = $event->show->matches;

        $p1 = $event->addPickForUser($user->id, $matches[0]->availablePicks[0]->id);
        $p2 = $event->addPickForUser($user->id, $matches[1]->availablePicks[0]->id);

        $this->assertEquals(2, $p1->confidence);
        $this->assertEquals(1, $p2->confidence);

        // Act
        $p2->increaseConfidence();

        // Assert
        $this->assertEquals(2, $p2->fresh()->confidence);
        $this->assertEquals(1, $p1->fresh()->confidence);
    }
    /** @test */
    function it_can_decreate_the_confidence()
    {
        $event = $this->createEventWithMatches(2);
        $user = factory(\WrestlePool\User::class)->create();

        $pick1 = $event->addPickForUser($user->id, $event->show->matches[0]->availablePicks[0]->id);
        $pick2 = $event->addPickForUser($user->id, $event->show->matches[1]->availablePicks[0]->id);

        $this->assertEquals(2, $pick1->confidence);
        $this->assertEquals(1, $pick2->confidence);

        $pick1->decreaseConfidence();

        $this->assertEquals(1, $pick1->fresh()->confidence);
        $this->assertEquals(2, $pick2->fresh()->confidence);
    }

    /** @test */
    function it_cannot_increase_the_confidence_passed_the_total_number_of_matches_on_the_show()
    {
        $user = factory(WrestlePool\User::class)->create();

        $event = $this->createEventWithMatches(1);

        $pick = $event->addPickForUser($user->id, $event->show->matches[0]->availablePicks[0]->id);

        try {
            $pick->increaseConfidence();
        } catch (WrestlePool\Exceptions\ConfidenceUpperBoundException $e) {
            return false;
        }

        $this->fail('A ConfidenceUpperBoundException should have been thrown');
    }

    /** @test */
    public function it_cannot_have_a_confidence_lower_than_1()
    {
        // Arrange
        $user = factory(\WrestlePool\User::class)->create();
        $event = $this->createEventWithMatches(1);
        $pick = $event->addPickForUser($user->id, $event->show->matches[0]->availablePicks[0]->id);
        $this->assertEquals(1, $pick->confidence);

        // Act
        try {
            $pick->decreaseConfidence();
        } catch(\WrestlePool\Exceptions\ConfidenceLowerBoundException $exception) {
            return;
        }

        // Assert
        $this->fail('a ConfidenceLowerBoundException should have been thrown');
    }
}

