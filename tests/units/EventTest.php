<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use WrestlePool\Event;
use WrestlePool\Show;

class EventTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_can_add_a_pick_for_a_user()
    {
        // Arrange
        $user = factory(\WrestlePool\User::class)->create();
        $show = factory(Show::class)->create();
        $pool = factory(\WrestlePool\Pool::class)->create();
        $event = $pool->addEventForShow($show);
        $match = $show->addMatch(collect([factory(\WrestlePool\Wrestler::class)->create()]));
        $this->assertEquals(0, $event->picks()->count());

        // Act
        $pick = $event->addPickForUser($user->id, $match->availablePicks[0]->id);

        // Assert
        $this->assertInstanceOf(\WrestlePool\Pick::class, $pick);
        $this->assertEquals(1, $event->picks()->count());
    }

    /** @test */
    function the_confidence_level_depends_on_how_many_matches_are_on_the_card_vs_how_many_picks_have_been_made()
    {
        // Arrange
        $user = factory(\WrestlePool\User::class)->create();
        $show = factory(Show::class)->create();
        $pool = factory(\WrestlePool\Pool::class)->create();
        $event = $pool->addEventForShow($show);

        $match1 = $show->addMatch(collect([factory(\WrestlePool\Wrestler::class)->create()]));
        $match2 = $show->addMatch(collect([factory(\WrestlePool\Wrestler::class)->create()]));
        $match3 = $show->addMatch(collect([factory(\WrestlePool\Wrestler::class)->create()]));

        // Act
        $pick = $event->addPickForUser($user->id, $match1->availablePicks[0]->id);
        $this->assertEquals(3, $pick->confidence);
        //
        // Act
        $pick = $event->addPickForUser($user->id, $match2->availablePicks[0]->id);
        $this->assertEquals(2, $pick->confidence);
        // Act
        $pick = $event->addPickForUser($user->id, $match3->availablePicks[0]->id);
        $this->assertEquals(1, $pick->confidence);
    }

    /** @test */
    public function it_defaults_its_name_to_be_the_shows_name()
    {
        // Arrange
        $event = new Event();
        $event->show()->associate(Show::create(['name' => 'Show']));
        // Act

        // Assert
        $this->assertEquals('Show', $event->name);
    }

    /** @test */
    public function an_event_can_be_smushed_to_its_state()
    {
        // Arrange
        /** @var \WrestlePool\Pool $pool */
        $pool = factory(\WrestlePool\Pool::class)->create();
        /** @var Show $show */
        $show = factory(Show::class)->create(['name' => 'Show Name']);
        $wrestler = factory(\WrestlePool\Wrestler::class)->create(['name' => 'Steve Austin']);
        $match = $show->addMatch(collect([$wrestler]));

        $event = $pool->addEventForShow($show);
        // Act

        // Assert
        $this->assertEquals([
            'name'    => 'Show Name',
            'matches' => [
                ['available_picks' => [
                    'misc'      => [],
                    'wrestlers' => [
                        ['name' => 'Steve Austin', 'id' => 1],
                    ],
                ]],
            ],
        ], $event->toState());
    }
}
