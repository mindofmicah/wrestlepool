<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use WrestlePool\Show;

class ShowTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function a_show_can_add_matches_with_wrestlers()
    {
        // Arrange
        $show = factory(Show::class)->create();
        $wrestler1 = factory(\WrestlePool\Wrestler::class)->create();
        $wrestler2 = factory(\WrestlePool\Wrestler::class)->create();

        // Act
        $this->assertEquals(0, $show->matches()->count());
        $match = $show->addMatch(collect([$wrestler1, $wrestler2]));

        // Assert
        $this->assertInstanceOf(\WrestlePool\Match::class, $match);
        $this->assertEquals(1, $show->matches()->count(1));
        $this->assertEquals(2, $show->matches[0]->wrestlers()->count());

        $this->assertEquals($wrestler1->id, $show->matches[0]->wrestlers[0]->id);
        $this->assertEquals($wrestler2->id, $show->matches[0]->wrestlers[1]->id);

        $this->assertEquals($match->availablePicks()->get(), $match->wrestlers()->get());
    }

}
