<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CanGetEventStatusTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_match_information()
    {
        // Arrange
        /** @var \WrestlePool\User $user */
        $user = factory(\WrestlePool\User::class)->create();
        auth()->loginUsingId($user->id);

        /** @var \WrestlePool\Pool $pool */
        $pool = factory(WrestlePool\Pool::class)->create();
        $user->joinPool($pool);

        /** @var \WrestlePool\Show $show */
        $show = factory(\WrestlePool\Show::class)->create(['name' => 'Wrestle-palooza']);

        $wrestler_1 = factory(\WrestlePool\Wrestler::class)->create(['name' => 'William Regal']);
        $wrestler_2 = factory(\WrestlePool\Wrestler::class)->create(['name' => 'Fit Finlay']);

        $show->addMatch(collect([$wrestler_1, $wrestler_2]));

        $event = $pool->addEventForShow($show);

        // Act
        $this->json('get', '/api/v1/pools/' . $pool->id . '/events/' . $event->id);
        // Assert
//dd($this->response->getContent());
        $this->seeJsonSubset([
            'event' => [
                'name'    => 'Wrestle-palooza',
                'matches' => [
                    [
                        'available_picks' => [
                            'wrestlers' => [
                                ['name' => 'William Regal',],
                                ['name' => 'Fit Finlay'],
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    /** @test */
    public function match_information_will_contain_picks()
    {
        // Arrange

        // Act

        // Assert

    }
}
