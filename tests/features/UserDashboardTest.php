<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserDashboardTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function a_user_can_see_all_upcoming_events_for_their_pools()
    {
        // Arrange
        /** @var \WrestlePool\User */
        $user = factory(\WrestlePool\User::class)->create();
        auth()->loginUsingId($user->id);

        /** @var \WrestlePool\Show */
        $show = factory(\WrestlePool\Show::class)->create(['name'=>'Show Name']);

        /** @var Pool */
        $pool1 = factory(\WrestlePool\Pool::class)->create(['name'=>'Pool']);
        $pool2 = factory(\WrestlePool\Pool::class)->create(['name'=>'Other Pool']);

        $user->joinPool($pool1);
        $user->joinPool($pool2);

        $pool1->addEventForShow($show);
        $pool2->addEventForShow($show);

        // Act
        $this->visit('/dashboard');

        // Assert
        $this->see('Pool');
        $this->see('Other Pool');
    }
}
