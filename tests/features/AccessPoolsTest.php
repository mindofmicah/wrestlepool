<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccessPoolsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_view_all_of_the_pools_they_are_a_member_of()
    {
        // Arrange
        $user = factory(\WrestlePool\User::class)->create();
        auth()->loginUsingId($user->id);

        /* @var WrestlePool\Pool */
        $pool1 = factory(\WrestlePool\Pool::class)->create(['name'=> 'Pool Name']);
        $pool2 = factory(\WrestlePool\Pool::class)->create(['name'=> 'Another Pool Name']);

        $user->joinPool($pool1);
        $user->joinPool($pool2);

        // Act
        $this->visit('/pools');

        // Assert
        $this->see('Pool Name');
        $this->see('Another Pool Name');
    }

    /** @test */
    public function a_user_can_only_see_pools_they_belong_to()
    {
        // Arrange
        /** @var \WrestlePool\User */
        $user = factory(\WrestlePool\User::class)->create();
        auth()->loginUsingId($user->id);

        factory(\WrestlePool\Pool::class)->create(['name'=>'INVALID GROUP']);

        // Act
        $this->visit('/pools');

        // Assert
        $this->dontSee('INVALID GROUP');
    }
}
