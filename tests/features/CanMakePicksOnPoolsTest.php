<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CanMakePicksOnPoolsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_submit_picks_for_a_pool_they_belong_to()
    {
        // Arrange
        // We need a legit WWE Event
        // Create a pool for that event
        // Invite a user
        // ^-- we need a user for this
        // There needs to be match to add to
        $show = factory(WrestlePool\Show::class)->create();
        $match = $show->addMatch(collect([factory(\WrestlePool\Wrestler::class)->create(['name'=>'Joe Wrestler'])]));
        $pick_id = $match->availablePicks()->first()->id;

        $user = factory(WrestlePool\User::class)->create();
        $pool = factory(WrestlePool\Pool::class)->create();

        $event = $pool->addEventForShow($show);

        $this->assertEquals(0, $event->picks()->count());

        $this->json('post', '/api/v1/pools/' . $pool->id . ' /events/' . $event->id . '/picks/', [
            'user_id'  => $user->id,
            'pick_id'  => $pick_id,
        ]);
    
        // Assert
        $this->assertResponseStatus(201);
        $this->seeJson([
            'id'         => 1,
            'name'       => 'Joe Wrestler',
            'confidence' => 1,
        ]);

        $this->assertEquals(1, $event->fresh()->picks()->count());
        $this->assertEquals($user->id, $event->fresh()->picks[0]->user_id);
    }


    /** @test */
    public function a_picks_confidence_can_be_boosted()
    {
        $event = factory(WrestlePool\Event::class)->create();

        $event->show->addMatch(collect([factory(WrestlePool\Wrestler::class)->create()]));
        $event->show->addMatch(collect([factory(WrestlePool\Wrestler::class)->create()]));

        $pick = factory(WrestlePool\Pick::class)->create(['confidence'=>1, 'event_id'=>$event->id]);

        $this->json('post', '/api/v1/picks/' . $pick->id . '/confidence/increase');

        $this->seeJsonSubset(['confidence'=>2]);
    }
}
