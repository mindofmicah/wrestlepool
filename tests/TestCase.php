<?php

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Create an event for a given number of matches
     * @param int $number_of_matches
     * @return \WrestlePool\Event
     */
    protected function createEventWithMatches($number_of_matches = 1)
    {
        $event = factory(WrestlePool\Event::class)->create();

        $wrestler = factory(WrestlePool\Wrestler::class)->create();

        foreach (range(1, $number_of_matches) as $match_number) {
            $event->show->addMatch(collect([$wrestler]));
        }

        return $event;
    }
}
