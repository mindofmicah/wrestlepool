<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace WrestlePool{
/**
 * WrestlePool\Match
 *
 * @property int $id
 * @property int $show_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \WrestlePool\Show $show
 * @property-read \Illuminate\Database\Eloquent\Collection|\WrestlePool\AvailablePick[] $availablePicks
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Match whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Match whereShowId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Match whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Match whereUpdatedAt($value)
 */
	class Match extends \Eloquent {}
}

namespace WrestlePool{
/**
 * WrestlePool\Show
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WrestlePool\Match[] $matches
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Show whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Show whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Show whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Show whereUpdatedAt($value)
 */
	class Show extends \Eloquent {}
}

namespace WrestlePool{
/**
 * WrestlePool\Pool
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WrestlePool\Event[] $events
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pool whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pool whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pool whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pool whereUpdatedAt($value)
 */
	class Pool extends \Eloquent {}
}

namespace WrestlePool{
/**
 * WrestlePool\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WrestlePool\Pool[] $pools
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $readNotifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace WrestlePool{
/**
 * WrestlePool\Wrestler
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WrestlePool\AvailablePick[] $availablePicks
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Wrestler whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Wrestler whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Wrestler whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Wrestler whereUpdatedAt($value)
 */
	class Wrestler extends \Eloquent {}
}

namespace WrestlePool{
/**
 * WrestlePool\Event
 *
 * @property int $id
 * @property int $pool_id
 * @property int $show_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WrestlePool\Pick[] $picks
 * @property-read \WrestlePool\Show $show
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Event whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Event wherePoolId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Event whereShowId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Event whereUpdatedAt($value)
 */
	class Event extends \Eloquent {}
}

namespace WrestlePool{
/**
 * WrestlePool\AvailablePick
 *
 * @property int $id
 * @property int $match_id
 * @property int $pickable_id
 * @property string $pickable_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \WrestlePool\Match $match
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $pickable
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\AvailablePick whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\AvailablePick whereMatchId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\AvailablePick wherePickableId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\AvailablePick wherePickableType($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\AvailablePick whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\AvailablePick whereUpdatedAt($value)
 */
	class AvailablePick extends \Eloquent {}
}

namespace WrestlePool{
/**
 * WrestlePool\Pick
 *
 * @property int $id
 * @property int $event_id
 * @property int $user_id
 * @property int $pick_id
 * @property int $confidence
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \WrestlePool\Event $event
 * @property-read \WrestlePool\AvailablePick $selection
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pick whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pick whereEventId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pick whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pick wherePickId($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pick whereConfidence($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pick whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\WrestlePool\Pick whereUpdatedAt($value)
 */
	class Pick extends \Eloquent {}
}

